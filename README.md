# TT Network library by Tritem

<!-- ## General information

_placeholder: to be added in the future_ -->

## Dependency

This project depends on following packages:

- [`tt-mainpalette`](https://gitlab.com/tt_libraries/tt-mainpalette)
- [`tt-controls`](https://gitlab.com/tt_libraries/tt-controls)
- [`tt-datamanipulation`](https://gitlab.com/tt_libraries/tt-datamanipulation)
- [`tt-error`](https://gitlab.com/tt_libraries/tt-error)
- [`tt-file`](https://gitlab.com/tt_libraries/tt-file)
- [`tt-utilities`](https://gitlab.com/tt_libraries/tt-utilities)

## Notes

### Automatic installation

Using _Chocolatey_ - versions up to `1.4.0`

`choco install tt-network -s "'https://gitlab.com/api/v4/projects/51817414/packages/nuget/v2'"`

Using _Chocolatey_ - versions from `2.0.0`

`choco install tt-network -s "'https://gitlab.com/api/v4/projects/51817414/packages/nuget/index.json'"`

or

`choco install tt-network -s "'https://gitlab.com/api/v4/groups/74269373/-/packages/nuget/index.json'"`

### Manual installation

Go to `https://gitlab.com/tt_libraries/tt-network/-/packages` and download the latest version of package. Then, using command prompt:

```powershell
cd <path-to-folder-with-downloaded-package>
choco install tt-network -s .
```

Use `-f` flag to force installation procedure.

Installation script **overwrites** content of `vi.lib\Tritem\Network` folder!

Uninstall procedure **removes** entire `vi.lib\Tritem\Network` folder!
